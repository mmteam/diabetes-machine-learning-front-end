import React, { Component } from 'react';
import Prediccion from './predicciones/js/Prediccion'
import 'bulma/css/bulma.css';

class App extends Component {
  render() {
    return (
      <div style={{background:"#f06292",height:"100vh"}}>
        <Prediccion />
      </div>
    );
  }
}

export default App;
