import React, { Component } from 'react'
import axios from 'axios';
import {
    Section,
    Container,
    Box,
    Columns,
    Column,
    Title,
    Label,
    Field,
    Control,
    Icon,
    Input,
    Button,
    Progress
} from 'bloomer'
const styles = {
    label:{
        color:'#757575'
    },
    section:{
       
    },
    info:{
        color:"#209cee"
    },
    titleMain:{
        color:'white'
    },
    allPage:{
        height:"95vh"
    },
    box:{
        background:'#00d1b2'
    }
  }
function ResultadoList(self,result) {
const listItems = result.map((resultado) =>
    <Columns>
        <Column isSize="1/4">
            <Label style={styles.label}>Época</Label>
            <Title  isSize={4}>{resultado.epoca}</Title>
        </Column>
        <Column isSize="1/4">
            <Label style={styles.label}>Pérdida</Label>
            <Title  isSize={4}>{resultado.epoca}</Title>
        </Column>
        <Column isSize="1/4">
            <Label style={styles.label}>Eficiencia</Label>
            <Title  isSize={4}>{resultado.eficiencia}</Title>
        </Column>
    </Columns>
);
self.setState({
    isLoading:false
});
return (
    <div style={{overflowY:"scroll",height:"40vh"}}>
    {listItems}
    </div>
);
}
export default class Prediccion extends Component {
    constructor(props){
        super(props)
        this.state={
            isLoading:false,
            disabled:false,
            resultados:null,
            prediccion:null,
            colorPositivo:'#ff4081',
            colorNegativo:'#00c853',
            colorResultado:null,
            embarazos:null,
            glucosa:null,
            presionSanguinea:null,
            grosorPiel:null,
            insulina:null,
            imc:null,
            pedigri:null,
            edad:null,
            tiempo:null,
            timeProgress:0,
            first:true
        }
        this.handleClick = this.handleClick.bind(this)
        this.handleEmbarazos = this.handleEmbarazos.bind(this)
        this.handleGlucosa = this.handleGlucosa.bind(this)
        this.handlePresionSanguinea = this.handlePresionSanguinea.bind(this)
        this.handleGrosorPiel = this.handleGrosorPiel.bind(this)
        this.handleInsulina = this.handleInsulina.bind(this)
        this.handleImc = this.handleImc.bind(this)
        this.handlePedigri = this.handlePedigri.bind(this)
        this.handleEdad = this.handleEdad.bind(this)
        this.timer = this.timer.bind(this)
    }
    componentDidMount(){
        var intervalId= setInterval(this.timer,40);
     this.setState({intervalId: intervalId});
    }

    componentWillUnmount(){
        clearInterval(this.state.intervalId);
    }

    timer(){
    // setState method is used to update the state
        if (this.state.timeProgress < 100){
            this.setState({
                timeProgress: this.state.timeProgress + 1,
            });
        }else{
            this.setState({
                timeProgress:0
            });
        }
    }
    handleClick() {
        this.setState({
            isLoading: !this.state.isLoading,
            first:false
        });
        console.log(this.state.grosorPiel)
        var self=this;
        let json = {
            "embarazos":this.state.embarazos,
            "glucosa":this.state.glucosa,
            "presionSanguinea":this.state.presionSanguinea,
            "grosorPiel":this.state.grosorPiel,
            "insulina":this.state.insulina,
            "imc":this.state.imc,
            "pedigri":this.state.pedigri,
            "edad":this.state.edad
        }
        let tInicial = performance.now();
        let tFinal;
        axios.post(
            "http://127.0.0.1:3001/get-resultados",
            json
            )
        .then(function(response){
            tFinal = performance.now();
            let resultado = response.data.prediccion[0].resultado
            self.setState({
                resultados: ResultadoList(self,response.data.resultados),
                prediccion: resultado,
                tiempo: (tFinal - tInicial)/1000
            }); 
            if (resultado === "POSITIVO"){
                self.setState({
                    colorResultado: self.state.colorPositivo
                });
            }else{
                self.setState({
                    colorResultado: self.state.colorNegativo
                });
            }
        });
    }

    handleGlucosa(value){
        this.setState({
            glucosa:value
        });
    }
    handleEmbarazos(value){
        this.setState({
            embarazos:value
        });
    }
    handlePresionSanguinea(value){
        this.setState({
            presionSanguinea:value
        });
    }
    handleGrosorPiel(value){
        this.setState({
            grosorPiel:value
        });
    }
    handleInsulina(value){
        this.setState({
            insulina:value
        });
    }
    handleImc(value){
        this.setState({
            imc:value
        });
    }
    handlePedigri(value){
        this.setState({
            pedigri:value
        });
    }
    handleEdad(value){
        this.setState({
            edad:value
        });
    }
    

  render() {
    const isLoading = this.state.isLoading;
    const isFirst = this.state.first;
    return (
        <Section style={styles.section}>
        <Container>
            <Box style={styles.box}>
                <Columns>
                    <Column hasTextAlign='left' isSize="1/2">
                        <Title style={styles.titleMain}isSize={1}>DIABETES EN MUJERES </Title>
                    </Column>
                    <Column hasTextAlign='right'>
                        <Title style={styles.titleMain}isSize={3}>Machine Learning</Title>
                        
                    </Column>
                </Columns>
                <Columns>
                <Column isSize="1/2">
                    <Box>
                        <Title isSize={3}>Datos del paciente</Title>
                        <Columns>
                            <Column >
                                <Columns>
                                    <Column isSize="1/2">
                                        <Field>
                                            <Label>Número de embarazos</Label>
                                            <Control>
                                                <Input type="number" onChange={(e) => this.handleEmbarazos(e.target.value)}/>
                                            </Control>
                                        </Field>
                                    </Column>
                                    <Column isSize="1/2">
                                        <Field>
                                            <Label>Glucosa</Label>
                                            <Control>
                                                <Input type="number" onChange={(e) => this.handleGlucosa(e.target.value)}/>
                                            </Control>
                                        </Field>
                                    </Column>
                                </Columns>
                                <Columns >
                                    <Column isSize="1/2">
                                        <Field>
                                            <Label>Presión sanguínea</Label>
                                            <Control>
                                                <Input type="number" onChange={(e) => this.handlePresionSanguinea(e.target.value)}/>
                                            </Control>
                                        </Field>
                                    </Column>
                                    <Column isSize="1/2">
                                        <Field>
                                            <Label>Grosor de la piel</Label>
                                            <Control>
                                                <Input type="number" onChange={(e) => this.handleGrosorPiel(e.target.value)}/>
                                            </Control>
                                        </Field>
                                    </Column>
                                </Columns>
                                <Columns>
                                    <Column isSize="1/2">
                                        <Field>
                                            <Label>Insulina</Label>
                                            <Control>
                                                <Input type="number" onChange={(e) => this.handleInsulina(e.target.value)} />
                                            </Control>
                                        </Field>
                                    </Column>
                                    <Column isSize="1/2">
                                        <Field>
                                            <Label>IMC</Label>
                                            <Control>
                                                <Input type="number" onChange={(e) => this.handleImc(e.target.value)}/>
                                            </Control>
                                        </Field>
                                    </Column>
                                </Columns>
                                <Columns>
                                    <Column isSize="1/2">
                                        <Field>
                                            <Label>Función de pedigrí</Label>
                                            <Control>
                                                <Input type="number" onChange={(e) => this.handlePedigri(e.target.value)} />
                                            </Control>
                                        </Field>
                                    </Column>
                                    <Column isSize="1/2">
                                        <Field>
                                            <Label>Edad</Label>
                                            <Control>
                                                <Input type="number" onChange={(e) => this.handleEdad(e.target.value)}/>
                                            </Control>
                                        </Field>
                                    </Column>
                                </Columns>
                                    <Field>
                                        <Column hasTextAlign='right'>
                                            <Button disabled={this.state.disabled} onClick={()=>this.handleClick(this)} isLoading={this.state.isLoading} style={{background:"#ff4081"}}isSize='large' isColor='success' >Diagnosticar</Button>
                                        </Column>
                                    </Field>
                            </Column>
                        </Columns>
                    </Box>
                </Column>
                {isLoading ? (
                    <Column isSize="1/2">
                        <Box style={{height:"75vh"}}>
                            <Title isSize={3} hasTextAlign="centered">Diagnosticando</Title>
                            <Progress isColor='primary' value={this.state.timeProgress} max={100}/>
                        </Box>
                    </Column>
                    ) : (
                    isFirst?(
                        <Column isSize="1/2">
                        <Box style={{height:"75vh"}}>
                            <Icon style={{paddingTop:"20em",paddingLeft:"16vw",color:"#ff4081"}}isSize="large" className="fas fa-venus fa-10x" />
                        </Box>
                    </Column>
                    ):(
                        <Column isSize="1/2">
                            <Box style={{height:"75vh"}}>
                                <Title isSize={3}>Resultados</Title>
                                <hr style={{color:"black"}}></hr>
                                <Title isSize={4} hasTextAlign='left'>Tiempo de procesamiento: {this.state.tiempo} seg</Title>
                                <hr style={{color:"black"}}></hr>
                                <Title isSize={4}>Métricas del entrenamiento</Title>
                                    {this.state.resultados}
                                    <hr style={{color:"black"}}></hr>
                                <Column hasTextAlign='centered'>
                                    <Title isSize={3} style={{color:this.state.colorResultado,marginTop:"-1em"}}>{this.state.prediccion}</Title>
                                </Column>
                            </Box>
                        </Column>
                    )
                )}
                </Columns>
            </Box>
        </Container>
    </Section>
    )
  }
}
